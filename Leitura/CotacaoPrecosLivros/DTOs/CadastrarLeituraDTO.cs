﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Leitura.DTOs
{
    public class CadastrarLeituraDTO
    {
        public string eMail { get; set; }
        public string ISBN { get; set; }
        public int PaginasLidas { get; set; }

    }
}