﻿using Leitura.DTOs;
using Leitura.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Leitura.Controllers
{
    [RoutePrefix("api/v1/leituras")]
    public class LeituraController : ApiController
    {
        private static List<HistoricoLeitura> listaLeituras = new List<HistoricoLeitura>();

        /// <summary>
        /// Cadastra ou atualiza o acompanhamento da leitura de um livro de um usuário.
        /// </summary>
        /// <param name="leitura">Todos os dados necessários para o cadastramento de uma leitura.</param>
        /// <returns>Retorna os dados atualizados do acompanhamento de leitura.</returns>
        [HttpPost]
        [Route("")]
        public HistoricoLeitura CadastrarLeitura(CadastrarLeituraDTO leitura)
        {
            if (string.IsNullOrEmpty(leitura.eMail))
                throw new Exception("Informe um e-Mail válido.");

            if (string.IsNullOrEmpty(leitura.ISBN))
                throw new Exception("Informe um ISBN.");

            Livro livro = new Livro();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:10873/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string uriObterLivroPorISBN = string.Format("http://localhost:10873/api/v1/livros/isbn/{0}", leitura.ISBN);
            var response = client.GetAsync(uriObterLivroPorISBN).Result;
            livro = JsonConvert.DeserializeObject<Livro>(response.Content.ReadAsStringAsync().Result);
            livro.PaginasLidas = leitura.PaginasLidas;

            HistoricoLeitura leitor = listaLeituras.SingleOrDefault(c => c.eMail == leitura.eMail);
            if (leitor == null)
            {
                leitor = new HistoricoLeitura();
                leitor.eMail = leitura.eMail;
                leitor.Livros = new List<Livro>();

                var novoLivro = new Livro();
                novoLivro.ISBN10 = livro.ISBN10;
                novoLivro.ISBN13 = livro.ISBN13;
                novoLivro.Titulo = livro.Titulo;
                novoLivro.Descricao = livro.Descricao;
                novoLivro.Paginas = livro.Paginas;
                novoLivro.PaginasLidas = leitura.PaginasLidas;
                leitor.Livros.Add(novoLivro);

                leitor.TotalPaginasLidas = leitura.PaginasLidas;
                listaLeituras.Add(leitor);
            }else
            {
                var aux = leitor.Livros.SingleOrDefault(l => l.ISBN10 == leitura.ISBN || l.ISBN13 == leitura.ISBN);
                if (aux != null)
                {
                    aux.PaginasLidas = leitura.PaginasLidas;                    
                }else
                {
                    var novoLivro = new Livro();
                    novoLivro.ISBN10 = livro.ISBN10;
                    novoLivro.ISBN13 = livro.ISBN13;
                    novoLivro.Titulo = livro.Titulo;
                    novoLivro.Descricao = livro.Descricao;
                    novoLivro.Paginas = livro.Paginas;
                    novoLivro.PaginasLidas = leitura.PaginasLidas;
                    leitor.Livros.Add(novoLivro);
                }
                leitor.TotalPaginasLidas = leitor.Livros.Sum(p => p.PaginasLidas);
            }

            return listaLeituras.SingleOrDefault(e => e.eMail == leitura.eMail);
        }

        /// <summary>
        /// Obtém uma lista de todos os livros de um usuário que foram completamente lidos.
        /// </summary>
        /// <param name="email">Email do usuário.</param>
        /// <returns>Obtém uma lista de todos os livros de um usuário que foram completamente lidos.</returns>
        [HttpGet]
        [Route("livrosLidos")]
        public IEnumerable<Livro> ObterLivrosLidos(string email)
        {
            if (string.IsNullOrEmpty(email))
                throw new Exception("Informe um e-Mail válido.");

            var leitura = listaLeituras.SingleOrDefault(e => e.eMail == email);

            if (leitura == null)
                throw new Exception("Não foi encontrado um usuário com o e-Mail informado.");

            return leitura.Livros.Where(p => p.PaginasLidas == p.Paginas);
        }

        /// <summary>
        /// Obtém uma lista de todos os livros de um usuário que foram parcialmente lidos.
        /// </summary>
        /// <param name="email">Email do usuário.</param>
        /// <returns>Obtém uma lista de todos os livros de um usuário que foram parcialmente lidos.</returns>
        [HttpGet]
        [Route("livrosParcialmenteLidos")]
        public IEnumerable<Livro> ObterLivrosParcialmenteLidos(string email)
        {
            if (string.IsNullOrEmpty(email))
                throw new Exception("Informe um e-Mail válido.");

            var leitura = listaLeituras.SingleOrDefault(e => e.eMail == email);

            if (leitura == null)
                throw new Exception("Não foi encontrado um usuário com o e-Mail informado.");

            return leitura.Livros.Where(p => p.PaginasLidas < p.Paginas);
        }

        /// <summary>
        /// Obtém todo o hisórico de leitura de um usuário.
        /// </summary>
        /// <param name="email">Email do usuário.</param>
        /// <returns>Obtém todo o hisórico de leitura de um usuário.</returns>
        [HttpGet]
        [Route("")]
        public HistoricoLeitura ObterHistoricoLeitura(string email)
        {
            if (string.IsNullOrEmpty(email))
                throw new Exception("Informe um e-Mail válido.");

            var leitura = listaLeituras.SingleOrDefault(e => e.eMail == email);

            if (leitura == null)
                throw new Exception("Não foi encontrado um usuário com o e-Mail informado.");

            return leitura;
        }

        /// <summary>
        /// Obtém o percentual lide de um livro específico de um usuário específico.
        /// </summary>
        /// <param name="eMail">Email do uauário.</param>
        /// <param name="ISBN">ISBN do livro.</param>
        /// <returns>Obtém o percentual lide de um livro específico de um usuário específico.</returns>
        [HttpGet]
        [Route("usuarios/{eMail}/livros/{ISBN}/percentuaisLidos")]
        public double ObterPercentualLido(string eMail, string ISBN)
        {
            if (string.IsNullOrEmpty(ISBN))
                throw new Exception("Informe um ISBN.");

            if (string.IsNullOrEmpty(eMail))
                throw new Exception("Informe um ISBN.");

            var livro = listaLeituras?.SingleOrDefault(e => e.eMail == eMail).Livros?.SingleOrDefault(i => i.ISBN10 == ISBN || i.ISBN13 == ISBN);
            if (livro == null)
                throw new Exception("Usuário ou livro não encontrado.");

            return ((double)livro.PaginasLidas / (double)livro.Paginas);
        }

    }
}
