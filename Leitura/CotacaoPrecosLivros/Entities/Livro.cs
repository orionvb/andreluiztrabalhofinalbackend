﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Leitura.Entities
{
    public class Livro
    {
        private int _paginasLidas;
        public string ISBN10 { get; set; }
        public string ISBN13 { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public int Paginas { get; set; }
        public int PaginasLidas
        {
            get
            {
                return _paginasLidas;
            }
            set
            {
                if (value > Paginas)
                    throw new Exception("Número de paginas lidas não pode ser superior ao número de páginas do livro.");

                _paginasLidas = value;
            }
        }
       
    }
}