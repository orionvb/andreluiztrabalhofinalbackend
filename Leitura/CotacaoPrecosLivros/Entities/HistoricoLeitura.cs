﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Leitura.Entities
{
    public class HistoricoLeitura
    {
        public string eMail { get; set; }
        public int TotalPaginasLidas { get; set; }
        public List<Livro> Livros { get; set; }
    }
}