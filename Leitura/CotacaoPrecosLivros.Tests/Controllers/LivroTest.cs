﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Leitura.Entities;

namespace CotacaoPrecosLivros.Tests.Controllers
{
    [TestClass]
    public class LivroTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            Livro livro = new Livro();
            livro.Paginas = 100;
            try
            {
                livro.PaginasLidas = 101;
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message == "Número de paginas lidas não pode ser superior ao número de páginas do livro.");
            }
            
        }
    }
}
