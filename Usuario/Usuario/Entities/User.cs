﻿using System.Collections.Generic;

namespace Usuarios.Entities
{
    public class User
    {
        private string _email;
        public int IdUsuario { get; set; }
        public string Nome { get; set; }
        public string SobreNome { get; set; }
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new System.Exception("E-mail é obrigatório.");

                _email = value;
            }
        }
        public List<Livro> LivrosFavoritos { get; set; }

        public void ValidarParaSalvar()
        {
            if (string.IsNullOrEmpty(Nome) || string.IsNullOrEmpty(SobreNome))
            {
                throw new System.Exception("Nome e Sobrenome do usuário são obrigatórios.");
            }
            else if (string.IsNullOrEmpty(Email))
            {
                throw new System.Exception("E-mail é obrigatório.");
            }
        }
    }
}