﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Usuarios.Entities
{
    public class Livro
    {
        private string eMail { get; set; }
        public string ISBN10 { get; set; }
        public string ISBN13 { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public double? NotaUsuarios
        {
            get
            {                
                return ObterNotaUsuarios();
            }
            set {}
        }
        public double? PercentualLido
        {
            get
            {
                return ObterPercentualLido();
            }
            set{}
        }
        public IList<string> Criticas
        {
            get
            {                
                return ObterCriticas();
            }
            set{}
        }

        public void ObterDadosLivro(string eMail)
        {
            this.eMail = eMail;
            Criticas = new List<string>();
            NotaUsuarios = 0;
            PercentualLido = 0;
        }

        private List<string> ObterCriticas()
        {

            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://localhost:16098/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string uriObterLivroPorISBN = string.Format("http://localhost:16098/api/v1/criticas/{0}", ISBN10);
                var response = client.GetAsync(uriObterLivroPorISBN).Result;
                return JsonConvert.DeserializeObject<List<string>>(response.Content.ReadAsStringAsync().Result);
            }catch
            {
                return new List<string>();
            }

        }

        private double? ObterPercentualLido()
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://localhost:16240/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string uriObterLivroPorISBN = string.Format("http://localhost:16240/api/v1/leituras/usuarios/{0}/livros/{1}/percentuaisLidos", eMail, ISBN10);
                var response = client.GetAsync(uriObterLivroPorISBN).Result;
                return JsonConvert.DeserializeObject<double>(response.Content.ReadAsStringAsync().Result) * 100;
            }
            catch
            {
                return null;
            }
        }

        private double? ObterNotaUsuarios()
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://localhost:16024/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string uriObterLivroPorISBN = string.Format("http://localhost:16024/api/v1/pontuacoes/{0}", ISBN10);
                var response = client.GetAsync(uriObterLivroPorISBN).Result;
                return JsonConvert.DeserializeObject<double>(response.Content.ReadAsStringAsync().Result);
            }
            catch
            {
                return null;
            }
        }
    }
}