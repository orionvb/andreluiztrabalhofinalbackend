﻿using System.Collections.Generic;
using Usuarios.Entities;

namespace Usuario.DTOs
{
    public class CadastroUsuarioDTO
    {
        public string Nome { get; set; }
        public string SobreNome { get; set; }
        public string Email { get; set; }
    }
}