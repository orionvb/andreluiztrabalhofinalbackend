﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Usuario.DTOs;
using Usuarios.Entities;

namespace Usuarios.Controllers
{
    [RoutePrefix("api/v1/usuarios")]
    public class UsuarioController : ApiController
    {
        private static List<User> listaUsuarios = new List<User>();

        /// <summary>
        /// Cadastra um novo usuário.
        /// </summary>
        /// <param name="usuario">Todos os dados necessários para cadastro.</param>
        /// <returns>Usuário cadastrado.</returns>
        [HttpPost]
        public User Cadastrar(CadastroUsuarioDTO usuario)
        {
            var aux = listaUsuarios.Where(e => e.Email == usuario.Email).Count();

            if (aux > 0)
                throw new Exception("E-mail já cadastrado.");

            User novoUsuario = new User();
            novoUsuario.Nome = usuario.Nome;
            novoUsuario.SobreNome = usuario.SobreNome;
            novoUsuario.Email = usuario.Email;
            novoUsuario.ValidarParaSalvar();
            novoUsuario.IdUsuario = listaUsuarios.Count() == 0 ? 1 : listaUsuarios.Max(c => c.IdUsuario) + 1;
            listaUsuarios.Add(novoUsuario);
            return novoUsuario;
        }

        /// <summary>
        /// Obtém uma lista com todos os usuários cadastrados.
        /// </summary>
        /// <returns>Obtém uma lista com todos os usuários cadastrados.</returns>
        [HttpGet]
        public List<User> ObterTodos()
        {
            return listaUsuarios;
        }

        /// <summary>
        /// Adiciona um livro na lista de favoritos de um usuário específico.
        /// </summary>
        /// <param name="IdUsuario">Identificador único do usuário.</param>
        /// <param name="ISBN">ISBN do livro favorito a ser cadastrado.</param>
        /// <returns></returns>
        [HttpPut]
        [Route("livrosFavoritos/{IdUsuario}")]
        public User AdicionarLivroListaFavoritos(int IdUsuario, string ISBN)
        {
            var usuario = listaUsuarios.Where(e => e.IdUsuario == IdUsuario).First();
            if (usuario == null)
                throw new Exception("Não foi possível encontrar o usuário com o ID informado.");

            int aux = 0;

            if (usuario.LivrosFavoritos != null)
            {
                aux = usuario.LivrosFavoritos.Where(c => c.ISBN10 == ISBN || c.ISBN13 == ISBN).Count();
            }else
            {
                usuario.LivrosFavoritos = new List<Livro>();
            }
                

            if (aux > 0)
                throw new Exception("Livro já cadastrado na lista de favoritos do usuário.");

            Livro livro = new Livro();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:10873/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string uriObterLivroPorISBN = string.Format("http://localhost:10873/api/v1/livros/isbn/{0}", ISBN);
            var response = client.GetAsync(uriObterLivroPorISBN).Result;
            livro = JsonConvert.DeserializeObject<Livro>(response.Content.ReadAsStringAsync().Result);
            livro.ObterDadosLivro(usuario.Email);
            usuario.LivrosFavoritos.Add(livro);

            return usuario;
        }

        /// <summary>
        /// Deleta um usuário cadastrado.
        /// </summary>
        /// <param name="idUsuario">Identificador único do usuário cadastrado.</param>
        /// <returns>True caso deleção tenha êxito. False caso contrário.</returns>
        [HttpDelete]
        [Route("{idUsuario}")]
        public bool RemoverUsuario(int idUsuario)
        {
            var usuario = listaUsuarios.SingleOrDefault(i => i.IdUsuario == idUsuario);
            if(usuario == null)
            {
                throw new Exception("Usuário não encontrado.");
            }else
            {
                listaUsuarios.Remove(usuario);
            }
            return true;
        }

        /// <summary>
        /// Deleta um livro da lista de favoritos de um usuário específico.
        /// </summary>
        /// <param name="idUsuario">Identificador único do usuário.</param>
        /// <param name="ISBN">ISBN do livro a ser removido da lista de favoritos do usuário.</param>
        /// <returns>True caso deleção tenha êxito. False caso contrário.</returns>
        [HttpDelete]
        [Route("{idUsuario}/livrosFavoritos/{ISBN}")]
        public bool RemoverLivroListaFavoritosUsuario(int idUsuario, string ISBN)
        {
            var usuario = listaUsuarios.SingleOrDefault(i => i.IdUsuario == idUsuario);
            if (usuario == null)
            {
                throw new Exception("Usuário não encontrado.");
            }
            else
            {
                Livro livroFavorito;
                if (!string.IsNullOrEmpty(ISBN))
                {
                    livroFavorito = usuario.LivrosFavoritos.SingleOrDefault(i => i.ISBN10 == ISBN || i.ISBN13 == ISBN);
                    if(livroFavorito != null)
                    {
                        usuario.LivrosFavoritos.Remove(livroFavorito);
                    }else
                    {
                        throw new Exception("Livronão encontrado na lista de favoritos do usuário.");
                    }
                                        
                }else
                {
                    throw new Exception("É necessário informar um ISBN.");
                }
                
            }
            return true;
        }
  
    }
}
