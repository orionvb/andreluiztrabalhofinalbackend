﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Usuarios.Entities;

namespace Usuario.Tests.Controllers
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            User user = new User();
            try
            {
                user.Email = null;
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message == "E-mail é obrigatório.");
            }
        }

        [TestMethod]
        public void TestMethod2()
        {
            User user = new User();
            try
            {
                user.ValidarParaSalvar();
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message == "Nome e Sobrenome do usuário são obrigatórios.");
            }
        }

        [TestMethod]
        public void TestMethod3()
        {
            User user = new User();
            try
            {
                user.Nome = "João";
                user.SobreNome = "Castanheira";
                user.ValidarParaSalvar();
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message == "E-mail é obrigatório.");
            }
        }

    }
}
