﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PontuacaoLivros.Entities
{
    public class Livro
    {
        public string ISBN10 { get; set; }
        public string ISBN13 { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
    }
}