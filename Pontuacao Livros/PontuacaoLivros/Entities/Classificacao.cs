﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PontuacaoLivros.Entities
{
    public class Classificacao
    {
        private double _nota;
        public string ISBN10 { get; set; }
        public string ISBN13 { get; set; }
        public string Titulo { get; set; }
        public int QuantidadeAvaliacoes { get; set; }
        private int SomatorioAvaliacoes { get; set; }
        public double Nota {
            get
            {
                return _nota;
            }
            set
            {
                if (value < 1 || value > 5)
                    throw new Exception("Pontuação do livro deve ser entre 1 e 5");

                QuantidadeAvaliacoes++;
                SomatorioAvaliacoes += (int)value;

                _nota = (double)SomatorioAvaliacoes/(double)QuantidadeAvaliacoes;
            }
       }

        public void ValidarParaSalvar()
        {
            if (string.IsNullOrEmpty(ISBN10) || string.IsNullOrEmpty(ISBN13))
                throw new Exception("É necessário informar o código ISBN do livro para pontuá-lo.");

            if(Nota == 0)
                throw new Exception("É necessário informar uma nota para o livro para pontuá-lo.");
        }
    }
}