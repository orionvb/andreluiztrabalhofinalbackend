﻿using Newtonsoft.Json;
using PontuacaoLivros.DTOs;
using PontuacaoLivros.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace PontuacaoLivros.Controllers
{
    [RoutePrefix("api/v1/pontuacoes")]
    public class PontuacaoLivrosController : ApiController
    {
        private static List<Classificacao> classificacoes = new List<Classificacao>();

        /// <summary>
        /// Obtém todas as classificações de livros cadastrados.
        /// </summary>
        /// <returns>Obtém todas as classificações de livros cadastrados.</returns>
        [HttpGet]
        [Route("")]
        public List<Classificacao> ObterTodas()
        {
            return classificacoes;
        }

        /// <summary>
        /// Obtém a classificação geral dada para um livro.
        /// </summary>
        /// <param name="ISBN">ISBN do livro que se deseja obter a classificação.</param>
        /// <returns>Obtém a classificação geral dada para um livro.</returns>
        [HttpGet]
        [Route("{ISBN}")]
        public double ObterClassificacao(string ISBN)
        {
            return classificacoes.SingleOrDefault(i => i.ISBN10 == ISBN || i.ISBN13 == ISBN).Nota;
        }

        /// <summary>
        /// Obtém o livro mais bem classificado cadastrado.
        /// </summary>
        /// <returns>Obtém o livro mais bem classificado cadastrado.</returns>
        [HttpGet]
        [Route("melhorClassificado")]
        public Classificacao ObterLivroMaisBemClassificado()
        {
            return classificacoes.OrderByDescending(n => n.Nota).FirstOrDefault();
        }

        /// <summary>
        /// Obtém o livro mais mal classificado cadastrado.
        /// </summary>
        /// <returns>Obtém o livro mais mal classificado cadastrado.</returns>
        [HttpGet]
        [Route("piorClassificado")]
        public Classificacao ObterLivroMaisMalClassificado()
        {
            return classificacoes.OrderBy(n => n.Nota).FirstOrDefault();
        }

        /// <summary>
        /// Cadastra uma classificação para um livro.
        /// </summary>
        /// <param name="novaClassificacao">Todos os dados necessários para o cadastramento de uma classificação.</param>
        /// <returns>Retorna todos os dados da classificação cadastrada.</returns>
        [HttpPost]
        [Route("")]
        public Classificacao Cadastrar(CadastrarClassificacaoDTO novaClassificacao)
        {
            Livro livro = new Livro();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:10873/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string uriObterLivroPorISBN = string.Format("http://localhost:10873/api/v1/livros/isbn/{0}", novaClassificacao.ISBN);
            var response = client.GetAsync(uriObterLivroPorISBN).Result;
            livro = JsonConvert.DeserializeObject<Livro>(response.Content.ReadAsStringAsync().Result);

            Classificacao classificacao = classificacoes.SingleOrDefault(c => c.ISBN10 == novaClassificacao.ISBN || c.ISBN13 == novaClassificacao.ISBN);
            bool nova = false;

            if (classificacao == null)
            {
                classificacao = new Classificacao();
                nova = true;
            }
                
            classificacao.ISBN10 = livro.ISBN10;
            classificacao.ISBN13 = livro.ISBN13;
            classificacao.Titulo = livro.Titulo;
            classificacao.Nota = novaClassificacao.Nota;
            classificacao.ValidarParaSalvar();

            if(nova)
                classificacoes.Add(classificacao);

            return classificacao;
        }
    }
}
