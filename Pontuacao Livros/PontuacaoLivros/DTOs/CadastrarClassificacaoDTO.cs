﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PontuacaoLivros.DTOs
{
    public class CadastrarClassificacaoDTO
    {
        public string ISBN;
        public int Nota { get; set; }
    }
}