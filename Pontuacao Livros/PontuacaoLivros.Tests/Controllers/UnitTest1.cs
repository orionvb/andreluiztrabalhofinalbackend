﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PontuacaoLivros.Entities;

namespace PontuacaoLivros.Tests.Controllers
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Classificacao classificacao = new Classificacao();
            try
            {
                classificacao.Nota = 10;
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message == "Pontuação do livro deve ser entre 1 e 5");
            }
        }

        [TestMethod]
        public void TestMethod2()
        {
            Classificacao classificacao = new Classificacao();
            try
            {
                classificacao.ValidarParaSalvar();
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message == "É necessário informar o código ISBN do livro para pontuá-lo.");
            }
        }

        [TestMethod]
        public void TestMethod3()
        {
            Classificacao classificacao = new Classificacao();
            try
            {
                classificacao.ISBN10 = "teste";
                classificacao.ISBN13 = "teste";
                classificacao.ValidarParaSalvar();
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message == "É necessário informar uma nota para o livro para pontuá-lo.");
            }
        }
    }
}
