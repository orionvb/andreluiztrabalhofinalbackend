﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ISBN.Entities
{
    public class ItemISBN
    {
        private int _paginas = 0;
        public string ISBN10 { get; set; }
        public string ISBN13 { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public int Paginas
        {
            get
            {
                return _paginas;
            }
            set
            {
                if (value < 1)
                    throw new Exception("Livro deve ter pelo menos uma página.");

                _paginas = value;
            }
        }
    }
}