﻿using Google.Apis.Books.v1;
using Google.Apis.Books.v1.Data;
using Google.Apis.Services;
using ISBN.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace ISBN.Controllers
{
    [RoutePrefix("api/v1/livros")]
    public class LivrosController : ApiController
    {
        public static BooksService service = new BooksService(
               new BaseClientService.Initializer
               {
                   ApplicationName = "ISBNBookSearch",
                   ApiKey = "AIzaSyAgU1X59NG9dUoDG8uQT_ItW3gS3rfU0RA",
               });

        private IList<ItemISBN> listaLivros = new List<ItemISBN>();

        /// <summary>
        /// Obtém os dados de um livro pelo seu ISBN.
        /// </summary>
        /// <param name="isbn">ISBN do livro.</param>
        /// <returns>Obtém os dados de um livro pelo seu ISBN.</returns>
        [HttpGet]
        [Route("isbn/{isbn}")]
        public ItemISBN BuscarLivroPorISBN(string isbn)
        {
            var result = service.Volumes.List(isbn).Execute();

            if (result.Items != null)
            {
                var item = result.Items.FirstOrDefault();
                ItemISBN livro = new ItemISBN();

                if (item.VolumeInfo != null)
                {
                    if (item.VolumeInfo.Title != null)
                        livro.Titulo = item.VolumeInfo.Title;

                    if(item.VolumeInfo.IndustryIdentifiers != null && item.VolumeInfo.IndustryIdentifiers.Count >= 1)
                    {
                        if (item.VolumeInfo.IndustryIdentifiers[0].Identifier != null)
                            livro.ISBN10 = item.VolumeInfo.IndustryIdentifiers[0].Identifier;

                        if (item.VolumeInfo.IndustryIdentifiers.Count >= 2 && item.VolumeInfo.IndustryIdentifiers[1].Identifier != null)
                            livro.ISBN13 = item.VolumeInfo.IndustryIdentifiers[1].Identifier;
                    }

                    if (item.VolumeInfo.Description != null)
                        livro.Descricao = item.VolumeInfo.Description;

                    livro.Paginas = item.VolumeInfo.PageCount != null && item.VolumeInfo.PageCount.HasValue ? item.VolumeInfo.PageCount.Value : -1;
                }

                return livro;
            }else
            {
                throw new System.Exception("Não foi possível encontrar o livro pelo ISBN informado.");
            }            
        }

        /// <summary>
        /// Obtém os dados do livro pelo seu título.
        /// </summary>
        /// <param name="titulo">Título do livro.</param>
        /// <returns>Obtém os dados do livro pelo seu título.</returns>
        [HttpGet]
        [Route("titulos/{titulo}")]
        public IList<ItemISBN> BuscarLivroPorTitulo(string titulo)
        {
            var result = service.Volumes.List(string.Concat("intitle:", titulo)).Execute();

            if (result.Items != null)
            {               
                if (listaLivros != null && listaLivros.Count > 0)
                    listaLivros.Clear();

                foreach (var livro in result.Items)
                {
                    if (livro.VolumeInfo != null)
                    {
                        ItemISBN item = new ItemISBN();

                        if (livro.VolumeInfo.Title != null)
                            item.Titulo = livro.VolumeInfo.Title;

                        if (livro.VolumeInfo.IndustryIdentifiers != null && livro.VolumeInfo.IndustryIdentifiers.Count >= 1)
                        {
                            if (livro.VolumeInfo.IndustryIdentifiers[0].Identifier != null)
                                item.ISBN10 = livro.VolumeInfo.IndustryIdentifiers[0].Identifier;

                            if (livro.VolumeInfo.IndustryIdentifiers.Count >= 2 && livro.VolumeInfo.IndustryIdentifiers[1].Identifier != null)
                                item.ISBN13 = livro.VolumeInfo.IndustryIdentifiers[1].Identifier;
                        }

                        if (livro.VolumeInfo.Description != null)
                            item.Descricao = livro.VolumeInfo.Description;

                        item.Paginas = livro.VolumeInfo.PageCount != null && livro.VolumeInfo.PageCount.HasValue ? livro.VolumeInfo.PageCount.Value : -1;

                        listaLivros.Add(item);
                    }
                }

                return listaLivros;
            }
            else
            {
                throw new System.Exception("Não foi possível encontrar o livro pelo título informado.");
            }
        }
    }

}
