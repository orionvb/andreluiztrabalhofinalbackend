﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ISBN.Entities;

namespace ISBN.Tests.Controllers
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            ItemISBN item = new ItemISBN();
            try
            {
                item.Paginas = -1;
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message == "Livro deve ter pelo menos uma página.");
            }
        }
    }
}
