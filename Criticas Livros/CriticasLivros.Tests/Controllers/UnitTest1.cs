﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CriticasLivros.Entities;
using System.Collections.Generic;

namespace CriticasLivros.Tests.Controllers
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Item item = new Item();
            try
            {
                item.ValidarParaSalvar();
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message == "É necessário informar o código ISBN do livro para pontuá-lo.");
            }
        }

        [TestMethod]
        public void TestMethod2()
        {
            Item item = new Item();
            try
            {
                item.ISBN10 = "teste";
                item.ISBN13 = "teste";
                item.Criticas = new List<string>();
                item.ValidarParaSalvar();
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message == "Crítica precisa ter pelo menos 10 caracteres.");
            }
        }

    }
}
