﻿using CriticasLivros.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace CriticasLivros.Controllers
{
    [RoutePrefix("api/v1/criticas")]
    public class CriticaController : ApiController
    {
        private static List<Item> listaCriticasLivros = new List<Item>();

        /// <summary>
        /// Cadastra uma crítica para um livro específico.
        /// </summary>
        /// <param name="ISBN">ISBN do livro a ser criticado.</param>
        /// <param name="critica">Crítica a ser cadastrada para o livro.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("{ISBN}")]
        public Item CriticarLivro(string ISBN, string critica)
        {
            if (string.IsNullOrEmpty(ISBN))
                throw new Exception("É necessário informar um ISBN.");

            if (string.IsNullOrEmpty(critica) || critica.Length < 10)
                throw new Exception("É necessário informar uma crítica com mais de 10 caracteres.");

            Livro livro = new Livro();
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:10873/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string uriObterLivroPorISBN = string.Format("http://localhost:10873/api/v1/livros/isbn/{0}", ISBN);
            var response = client.GetAsync(uriObterLivroPorISBN).Result;
            livro = JsonConvert.DeserializeObject<Livro>(response.Content.ReadAsStringAsync().Result);

            var item = listaCriticasLivros.SingleOrDefault(c => c.ISBN10 == ISBN || c.ISBN13 == ISBN);
            if (item == null)
            {
                item = new Item();
                item.ISBN10 = livro.ISBN10;
                item.ISBN13 = livro.ISBN13;
                item.Titulo = livro.Titulo;
                item.Descricao = livro.Descricao;
                item.Criticas = new List<string>();
                item.ValidarParaSalvar();
                item.Criticas.Add(critica);
                listaCriticasLivros.Add(item);
            }else
            {
                item.ValidarParaSalvar();
                item.Criticas.Add(critica);
            }

            return item;
        }

        /// <summary>
        /// Obtém uma lista de todas as críticas cadastradas.
        /// </summary>
        /// <returns>Obtém uma lista de todas as críticas cadastradas.</returns>
        [HttpGet]
        [Route("")]
        public IList<Item> BuscarTodas()
        {
            return listaCriticasLivros;
        }

        /// <summary>
        /// Obtém todas as críticas de um livro específico.
        /// </summary>
        /// <param name="ISBN">ISBN do livro que se deseja obter as críticas.</param>
        /// <returns>Obtém todas as críticas de um livro específico.</returns>
        [HttpGet]
        [Route("{ISBN}")]
        public IList<string> ObterCriticasLivro(string ISBN)
        {
            var livro = listaCriticasLivros.SingleOrDefault(i => i.ISBN10 == ISBN || i.ISBN13 == ISBN);

            if (livro.Criticas == null)
                throw new Exception("Não existem críticas disponíveis para o título.");

            return livro.Criticas;
        }

    }
}
