﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CriticasLivros.Entities
{
    public class Item
    {
        public string ISBN10 { get; set; }
        public string ISBN13 { get; set; }
        public string Titulo { get; set; }
        public string Descricao { get; set; }
        public List<string> Criticas { get; set; }

        public void ValidarParaSalvar()
        {
            if (string.IsNullOrEmpty(ISBN10) || string.IsNullOrEmpty(ISBN13))
                throw new Exception("É necessário informar o código ISBN do livro para pontuá-lo.");

            if (Criticas == null)
                Criticas = new List<string>();

            foreach (var critica in Criticas)
            {
                if (string.IsNullOrEmpty(critica) || critica.Length < 10)
                    throw new Exception("Crítica precisa ter pelo menos 10 caracteres.");
            }

        }
    }
}